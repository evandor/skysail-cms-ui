import { h } from 'vue'
import { QBadge } from 'quasar'

export default {
  name: 'SkysailCmsUi',

  setup () {
    return () => h(QBadge, {
      class: 'SkysailCmsUi',
      label: 'SkysailCmsUi'
    })
  }
}
