import { h } from 'vue'
import { QBadge } from 'quasar'

export default {
  name: 'SkysailCmsUi2',

  setup () {
    return () => h(QBadge, {
      class: 'SkysailCmsUi',
      label: 'SkysailCmsUi2***'
    })
  }
}
