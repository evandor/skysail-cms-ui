import {cmsCore2} from "../../coreUi";
import {ContentBlockPosition, LayoutPart} from "skysail-cms-core";

export const onInput = (component, e) => {
    cmsCore2.pagesStore.contentChanged(component.id, e.target.innerText, component.content || '')
}


/**
 * to be used when the block accepts multiple elements.
 *
 */
export const addElement = (component, position, type, attributes) => {
    console.log("adding element: determined position", position)
    cmsCore2.pagesStore.addElement(position, component.id, type, attributes || {})
    //websitesStore.updatePage(pageStore.currentPage) - happens in pageStore add Element
}


/**
 * the block only accepts one element. An existing element should be replaced.
 */
export const handleDropEvent = (component, e) => {
    let position = ContentBlockPosition.AFTER
    if (e.added.newIndex === 0) {
        position = ContentBlockPosition.BEFORE
    }
    console.log(`drag&drop: adding element '${e.added.element.type}' ${position} ${component.type}(${component.id})`)
    cmsCore2.pagesStore.addElement(position, component.id, e.added.element.type)
}

export const setElement = (component, type, layoutPart, attributes) => {
    console.log("setting or replacing element / type / layout / attributes", component.id, type, layoutPart || LayoutPart.PAGE, attributes || {})
    cmsCore2.pagesStore.setElement(component.id, type, attributes)
}

export const addToAttributes = (component, attributes) => {
    console.log("common.ts: adding to attributes ", component.id, attributes)
    cmsCore2.pagesStore.addToAttributes(component.id, attributes)
    cmsCore2.websitesStore.updatePage(cmsCore2.pagesStore.currentPage)
    // TODO !!! cmsPagesApi.saveDraft(websitesStore.currentWebsite.id, pageStore.currentPage)
}

// const addDroptarget = (component, position: number): Promise<any> => {
//   console.log("common.js: adding droptarget", position, component.id)
//   const res = cmsCore2.pagesStore.addDroptarget(component.id, position)
//   cmsCore2.websitesStore.updatePage(cmsCore2.pagesStore.currentPage)
//   return res
// }
//
// const removeGridColumn = (component, position: number): Promise<any> => {
//   console.log("common.js: removing grid column", position, component.id)
//   const res = cmsCore2.pagesStore.removeGridColumn(component.id, position)
//   cmsCore2.websitesStore.updatePage(cmsCore2.pagesStore.currentPage)
//   return res
// }

export const setSingleElement = (component, event) => {
    console.log("replacing droptarget element / layout", event, "---")
    cmsCore2.pagesStore.setElement(component.id, event.added.element.type)
}


function addIfDefined(dyncClasses, attributes, ident, prefix = '') {
    if (attributes[ident]) {
        //console.log("adding", ident, attributes)
        dyncClasses.push(prefix + attributes[ident])
    }
}

export const dynamicPageClasses = (attributes) => {
    const dyncClasses = []
    if (attributes) {
        addIfDefined(dyncClasses, attributes, 'padding', 'q-pa-')
        addIfDefined(dyncClasses, attributes, 'margin', 'q-ma-')
        addIfDefined(dyncClasses, attributes, 'container')
        addIfDefined(dyncClasses, attributes, 'direction')
        addIfDefined(dyncClasses, attributes, 'justifyContent')
        addIfDefined(dyncClasses, attributes, 'border')
        addIfDefined(dyncClasses, attributes, 'fontWeight')
        addIfDefined(dyncClasses, attributes, 'align')
        addIfDefined(dyncClasses, attributes, 'class')
        // addIfDefined(dyncClasses, attributes, 'h')

        if (attributes['paddingDirection'] && attributes['paddingSize']) {
            dyncClasses.push(`q-p${attributes['paddingDirection']}-${attributes['paddingSize']}`)
        }
        if (attributes['marginDirection'] && attributes['marginSize']) {
            dyncClasses.push(`q-m${attributes['marginDirection']}-${attributes['marginSize']}`)
        }
    }
    return dyncClasses
}

export const selectElement = (component, e) => {
    console.log("selectedElement", component, e)
    cmsCore2.navigation.expansionItemSelectedElement = true
    cmsCore2.pagesStore.setCurrentSelectionById(component.id)
    if (!e.metaKey) {
        cmsCore2.pagesStore.unsetCurrentSelections()
    }
    cmsCore2.pagesStore.toggleCurrentSelectionsById(component.id)
    return
}

function handleStyle(attributes, styles, ident) {
    if (attributes && attributes[ident]) {
        let val = attributes[ident]
        styles.push(`${ident}:${val}`)
    }
}

export const getStyles = (attributes) => {
    let styles = []
    // console.log("attributes", attributes)

    if (attributes?.color) {
        let color = attributes.color
        if (color.indexOf('#') === 0) {
            styles.push(`color:${color}`)
        } else {
            styles.push(`color:${cmsCore2.websitesStore.currentWebsite.draftColors[color]}`)
        }
    }
    if (attributes && attributes['bgColor']) {
        //console.log("attributes", attributes)
        let color = attributes['bgColor']
        if (color.indexOf('#') === 0) {
            styles.push(`background-color:${color}`)
        } else {
            styles.push(`background-color:${cmsCore2.websitesStore.currentWebsite.draftColors[color]}`)
        }
    }
    handleStyle(attributes, styles, 'fontFamily')
    handleStyle(attributes, styles, 'fontWeight')

    if (attributes && attributes['style']) {
        _.forEach(_.filter(attributes['style'].split(';'), s => s && s.trim().length > 0), s => styles.push(s))
    }
    if (attributes && attributes['borders']) {
        _.forEach(
            _.map(
                _.filter(attributes['borders'].split(';'),
                    s => s && s.trim().length > 0), s => {
                    Object.keys(cmsCore2.websitesStore.currentWebsite.draftColors).forEach(function (key) {
                        const val = cmsCore2.websitesStore.currentWebsite.draftColors[key];
                        if (key && key.length > 3) { // getting key "s" ??
                            s = s.replace(key, val)
                        }
                    });
                    return s
                }),
            s => styles.push(s)
        )
    }

    if (attributes && attributes['setWidth'] && attributes['width'] && attributes['widthUnit']) {
        styles.push(`width: ${attributes['width']}${attributes['widthUnit']}`)
    }
    if (attributes && attributes['setHeight'] && attributes['height'] && attributes['heightUnit']) {
        styles.push(`height: ${attributes['height']}${attributes['heightUnit']}`)
    }

    return styles
}

export const animation = (attributes) => {
    if (attributes && attributes['animation']) {
        return "animated " + attributes['animation']
    }
    return ""
}

