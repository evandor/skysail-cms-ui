import Component from './components/Component'
import Component2 from './components/Component2'
import TestVue from './components/TestVue.vue'
import CmsHeading from './components/elements/cms-heading.vue'
import CmsParagraph from "./components/elements/cms-paragraph";
import {cmsCore2, initializeCmsUi} from './coreUi'
import CmsContentMenu from "./components/helper/cms-content-menu";
import CmsContentEditable from "./components/helper/cms-content-editable";
import {
    ClosePopup,
    QBtn,
    QCard,
    QCardActions,
    QCardSection,
    QDialog,
    QEditor,
    QIcon,
    QInput,
    QSelect,
    QTooltip
} from "quasar";
import _ from "lodash"
import CmsEditorDialog from "./components/helper/cms-editor-dialog";
import CmsColumns from "./components/elements/cms-columns";
import CmsRows from "./components/elements/cms-rows";
import CmsDroptarget from "./components/elements/cms-droptarget";
import CmsImg from "./components/elements/cms-img";
import CmsRootSection from "./components/elements/cms-root-section";
import CmsLink from "./components/elements/cms-link";
import CmsParallax from "./components/elements/cms-parallax";
import CmsRuler from "./components/elements/cms-ruler";
import CmsSection from "./components/elements/cms-section";
import CmsWebsiteHeader from "./components/layout/cms-website-header";
import CmsWebsiteFooter from "./components/layout/cms-website-footer";
import CmsText from "./components/elements/cms-text";
import CmsComment from "./components/helper/cms-comment";
import {eventEmitter} from "../dev/src/services/events";
import CmsHtml from "./components/elements/cms-html";
import CmsSimpleList from "./components/elements/cms-simple-list";

const version = __UI_VERSION__

function install(app) {
    _.forEach([
        CmsColumns, CmsDroptarget, CmsHeading, CmsHtml, CmsParagraph, CmsRootSection, CmsEditorDialog, CmsImg, CmsRows,
        CmsLink, CmsSimpleList, CmsParallax, CmsRuler, CmsSection, CmsWebsiteHeader, CmsWebsiteFooter, CmsText, CmsComment,
        QBtn, QCard, QCardActions, QCardSection, QDialog, QEditor, QIcon, QInput, QSelect, QTooltip
    ], element => {
        app.component(element.name, element)
    })
    app.component(Component.name, Component)
    app.component(Component2.name, Component2)
    app.component(TestVue.name, TestVue)
    app.component("CmsContentEditable", CmsContentEditable)
    app.component("CmsContentMenu", CmsContentMenu)

    app.directive("ClosePopup", ClosePopup)

    app.config.globalProperties.emitter = eventEmitter;
}

export {
    version,
    Component,
    cmsCore2,
    initializeCmsUi,
    install
}
