import {
    CmsCore, CmsWebsitesApi,
    ContentBlock,
    ContentBlockType,
    Flex,
    MainColors,
    Page,
    Website, WebsiteAndId
} from "skysail-cms-core";

export var cmsCore2 = null

// export const initializeCmsUi2 = (url: string, key: string, domain: string) => {
//     cmsCore2 = initializeCmsCore(url, key, domain);
// }

// @ts-ignore
//initializeCmsUi2(process.env.backendUrl,process.env.firebaseApiKey,process.env.firebaseAuthDomain)
//console.log("cms core set to ", cmsCore2)

export function initializeCmsUi(
    backendUrl,
    apiKey,
    authDomain
) {
    console.log("====================================================")
    console.log("=== initializing cmsCore in cmsUi with url", backendUrl)
    console.log("====================================================")
    cmsCore2 = new CmsCore(backendUrl, apiKey, authDomain)
    return cmsCore2
}

class MockCmsWebsitesApi extends CmsWebsitesApi {

    websiteWithId = null

    constructor() {
        super("mock", "mock");

        const websiteOne = new Website("websiteOneId", "websiteOne", "", "","title",
            new Flex("","","","","",""),
            null ,
            null )
        const elements = []
        elements.push(new ContentBlock("theId2", ContentBlockType.ContentBlockHeading, "content"))
        const pageOne = new Page("pageOneId", "namePageOne", 1, 0,0,0,elements)
        const pages = []
        pages.push(pageOne)
        websiteOne.setCurrentPages(pages)
        websiteOne.setOriginalPages(pages)
        this.websiteWithId = new WebsiteAndId("websiteOneId", websiteOne)

    }

    getWebsites() {
        console.log("mocking getWebsites call with", this.websiteWithId)
        return Promise.resolve({data: this.websiteWithId});
    }

    getWebsite(websiteId) {
        console.log("mocking getWebsites(id) call with", this.websiteWithId)
        return Promise.resolve(
            {
                data: {
                    "originalPages": [
                        {
                            "published": 1651300078007,
                            "dirty": false,
                            "created": 1647156111671,
                            "elements": [
                                {
                                    "id": "headingId",
                                    "type": "CmsHeading",
                                    "content": "content editable",
                                    "attributes": {
                                        "h": "5"
                                    }
                                }
                            ],
                            "version": 55,
                            "name": "index",
                            "changed": 1650963589322,
                            "id": "ec69fb8f-f6ba-4501-9852-f1455b3e390f"
                        }
                    ],
                    "currentPages": [
                        {
                            "published": 1651300078007,
                            "name": "index",
                            "version": 63,
                            "created": 1647156111671,
                            "dirty": false,
                            "elements": [
                                {
                                    "id": "headingId",
                                    "type": "CmsHeading",
                                    "content": "first heading for test 1647156108397",
                                    "attributes": {
                                        "h": "3"
                                    }
                                },
                                {
                                    "id": "textId",
                                    "type": "CmsText",
                                    "elements": [],
                                    "element": null,
                                    "content": "content editable"
                                },
                                {
                                    "id": "columns2Id",
                                    "type": "CmsColumns",
                                    "elements": [

                                    ],
                                    "element": null,
                                    "attributes": {},
                                    "content": "new undefined"
                                },
                                {
                                    "id": "rowsId",
                                    "type": "CmsRows",
                                    "elements": [

                                    ],
                                    "element": null,
                                    "attributes": {},
                                    "content": "new undefined"
                                },
                                {
                                    "id": "imgId",
                                    "type": "CmsImg",
                                    "elements": [

                                    ],
                                    "element": null,
                                    "attributes": {},
                                    "content": "new undefined"
                                }
                            ],
                            "id": "ec69fb8f-f6ba-4501-9852-f1455b3e390f",
                            "changed": 1652164740171
                        }
                    ],
                    "flex": {
                        "draft": {
                            "direction": "column"
                        },
                        "original": {
                            "direction": "column"
                        }
                    },
                    "nav": [],
                    "bgColor": "#ffffff",
                    "fontColor": "#000000",
                    "bgImg": null
                }
            }
        );
    }
}

export function initializeMockCmsUi() {
    console.log("==========================================")
    console.log("=== initializing cmsCore Mock in cmsUi ===")
    console.log("==========================================")
    cmsCore2 = new CmsCore("mock", "mock", "mock")

    // const websiteOne = new Website("websiteOne", "websiteOneName", "","","title",
    //     null as unknown as Flex, null as unknown as MainColors, null as unknown as MainColors)
    // const elements: ContentBlock[] = []
    // elements.push(new ContentBlock("theId2", ContentBlockType.ContentBlockHeading, "content"))
    // const pageOne = new Page("pageOneId", "namePageOne", 1, 0,0,0,elements)
    // const pages: Page[] = []
    // pages.push(pageOne)
    // websiteOne.setCurrentPages(pages)
    // websiteOne.setOriginalPages(pages)
    // cmsCore2.websitesStore.setCurrentWebsite(websiteOne)
    const mockCmsWebsitesApi = new MockCmsWebsitesApi()
    cmsCore2.cmsWebsitesApi = mockCmsWebsitesApi
    cmsCore2.websitesStore.cmsWebsitesApi = mockCmsWebsitesApi
    const websites = []
    websites.push(mockCmsWebsitesApi.websiteWithId)
    cmsCore2.websitesStore.websites = websites
    cmsCore2.websitesStore.setCurrentWebsiteById("websiteOneId")

    return cmsCore2
}

export default {
    cmsCore2,
    initializeCmsUi
}

//module.exports = {initializeCmsUi2,cmsCore2}
