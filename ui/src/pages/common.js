export const pageStyleHeaderOrFooter = (website, apply) => {
    let styles = ''
    if (!apply) {
        return styles
    }
    if (!website?.draftFlex.noMaxWidth && website?.draftFlex?.maxWidth) {
        styles += 'max-width:'+website?.draftFlex?.maxWidth+'px;'
    }
    if (website?.draftFlex?.marginLeftAuto) {
        styles += 'margin-left:auto;'
    } else {
        styles += 'margin-left:'+website?.draftFlex?.marginLeft+'px;'
    }
    if (website?.draftFlex?.marginRightAuto) {
        styles += 'margin-right:auto;'
    } else {
        styles += 'margin-right:'+website?.draftFlex?.marginRight+'px;'
    }
    return styles
}
