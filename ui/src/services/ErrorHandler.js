import {Notify} from 'quasar'

export function useNotificationHandler() {

    const handleError = (error) => {
        console.log("--- catched error ---")
        console.log("type", typeof error)
        console.log("msg", error)
        Notify.create({
            position: 'bottom',
            color: 'red-5',
            textColor: 'white',
            icon: 'error',
            message: error.toString()
        })
    }

    const handleSuccess = (msg) => {
        Notify.create({
            position: 'bottom',
            color: 'positive',
            textColor: 'white',
            icon: 'check',
            message: msg
        })
    }

    return {
        handleError, handleSuccess
    }
}
