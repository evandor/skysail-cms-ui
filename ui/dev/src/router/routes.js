
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/Main.vue') }],
  },
  {
    path: '/cms-heading',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CmsHeading.vue') }],
  },
  {
    path: '/cms-text',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CmsText.vue') }],
  },
  {
    path: '/cms-img',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CmsImg.vue') }],
  },
  {
    path: '/cms-html',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CmsHtml.vue') }],
  },
  {
    path: '/cms-simple-list',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CmsSimpleList.vue') }],
  },
  {
    path: '/cms-columns',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CmsColumns.vue') }],
  },
  {
    path: '/cms-rows',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CmsRows.vue') }],
  },
  {
    path: '/cms-root-section',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CmsRootSection.vue') }],
  },
  {
    path: '/cms-droptarget',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CmsDroptarget.vue') }],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
