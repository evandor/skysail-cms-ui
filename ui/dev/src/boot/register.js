import { boot } from 'quasar/wrappers'
import VuePlugin from 'ui' // "ui" is aliased in quasar.conf.js
import VueHighlightJS from 'vue3-highlightjs'
import 'highlight.js/styles/solarized-light.css'

export default boot(({ app }) => {
  app.use(VuePlugin)
  app.use(VueHighlightJS)
})
