// import {prettier, prettierPlugins} from 'standalone.js'
// import prettier from 'standalone.js';
import _ from "lodash";

const prettier = require("prettier");
const plugins = [require("prettier/parser-html")];
import hljs from 'highlight.js';
import {unref} from "vue";

const renderInto = (from: string, to: string) => {
  const cmsText = document.getElementById(from)
  const out = document.getElementById(to)
  if (out && cmsText) {
    out.innerHTML = hljs.highlight(cmsText.outerHTML, {language: 'html'}).value//cmsText.outerHTML
  }
}

function nthParent(number: number, element: HTMLElement) {
  let parent = element
  for (let i = 0; i < number; i++) {
    parent = parent.parentElement || element
  }
  return parent
}

const renderParentInto = (prefix: string, from: string, levelsUp: number, to: string) => {
  const out = document.getElementById(to)
  if (out) {
    out.innerHTML = ''
  } else {
    console.error(`renderParentInto: could not find 'to' by getElementById('${to}')`)
    return
  }

  const cmsText = document.getElementById(prefix + from);
  if (cmsText) {
    const parent = nthParent(levelsUp, cmsText)
    //alert(parent.outerHTML)
    const code = prettier.format(parent.outerHTML
        .replace(/<!\-\-\-\->/g, "")
        .replace(/<!\-\-v\-if\-\->/g, ""),
      {
        parser: "html",
        plugins,
        htmlWhitespaceSensitivity: "ignore"
      })
    out.innerHTML = hljs.highlight(code, {language: 'html'}).value//.replace("<!---->","")
  } else {
    console.error(`renderParentInto: could not find getElementById('${prefix + from}')`)
    //console.error(`details: prefix: '${prefix}', from: '${from}', to: '${to}'`)
  }
}

const renderCodeInto = (code: string, to: string) => {
  const out = document.getElementById(to)
  if (out) {
    out.innerHTML = ''
  }
  const cmsText = code;
  if (out && cmsText) {

    //const parent = nthParent(0, cmsText)
    //alert(parent.outerHTML)
    const pretty = prettier.format(code
        .replace(/<!\-\-\-\->/g, "")
        .replace(/<!\-\-v\-if\-\->/g, ""),
      {
        parser: "html",
        plugins,
        htmlWhitespaceSensitivity: "ignore"
      })
    out.innerHTML = hljs.highlight(pretty, {language: 'html'}).value//.replace("<!---->","")
  } else {
    console.error("problem with out/cmsText", out, cmsText)
    console.error("details: to", to)
  }
}

interface Attrs {
  [key: string]: any;
}

const getAttributesFrom = (attributeMap: any) => {
  //console.log("attributeMap!", attributeMap, typeof attributeMap)
  let attrs: Attrs = {}
  unref(attributeMap).forEach((value: any, key: any) => {
    if ('none' !== value && '' !== value) {
      attrs[key] = value
    }
  })
  return attrs;
}

export {
  renderInto,
  renderParentInto,
  renderCodeInto,
  getAttributesFrom
}
