
const mitt = require('mitt')
const eventEmitter = mitt()

export {
  eventEmitter
}
