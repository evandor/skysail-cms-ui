import {describe, expect, it} from '@jest/globals';
import {installQuasarPlugin} from '@quasar/quasar-app-extension-testing-unit-jest';
import {shallowMount} from '@vue/test-utils';
import {AxiosResponse} from "axios";
import CmsHeading from "pages/CmsHeading.vue";

installQuasarPlugin();

// jest.mock('store/auth', () => ({
//   __esModule: true, // this property makes it work
//   default: 'mockedDefaultExport'
// }));
//
// jest.mock('boot/firebase', () => ({
//   __esModule: true, // this property makes it work
//   default: 'mockedDefaultExport'
//  // getToken: jest.fn(() => {Promise.resolve()}),
// }));
//
// jest.mock('services/ErrorHandler', () => ({
//   __esModule: true, // this property makes it work
//   default: 'mockedDefaultExport',
//   useNotificationHandler: jest.fn(() => {
//     const handleError = () => {}
//     return {handleError}
//   }),
// }));

let defaultProps = {
  id: "theId",
  attributes: {h: 2},
  editable: true
};

describe('CmsHeading', () => {

  //const cmsPagesApi = initializeCmsCore("","","").cmsPagesApi

  // @ts-ignore
  // const saveDraftSpy = jest.spyOn(cmsPagesApi, 'saveDraft').mockImplementation((w: string, p: Page) => {
  //   return Promise.resolve() as unknown as Promise<AxiosResponse<any>>
  // })
  //
  // // @ts-ignore
  // const deletePageBlockSpy = jest.spyOn(cmsPagesApi, 'deletePageBlock').mockImplementation((wId: string, pId: string, bid:string) => {
  //   return Promise.resolve() as unknown as Promise<AxiosResponse<any>>
  // })

  //const notifyCreateSpy = jest.spyOn(Notify, 'create').mockImplementation((opts: object) => {  })
  //const successToastSpy = jest.spyOn(useNotificationHandler, 'handleSuccess').mockImplementation((msg: string) => {})

  // const addChangeSpy = jest.spyOn(cmsCore.cmsCore.changesStore, 'addChange')

  beforeEach(() => {
    // jest.resetModules();
    // cmsCore.cmsCore.pagesStore.currentSelection = new ContentBlock("initialHeadingBlockId", ContentBlockType.ContentBlockHeading, "currentSelectionContent")
    // cmsCore.cmsCore.pagesStore.currentPage = new Page("pageId", "name", 0, 0, 0, 0, [cmsCore.cmsCore.pagesStore.currentSelection])
    // Object.defineProperty(cmsCore.cmsCore.websitesStore, 'currentWebsite', {
    //   get: () => new Website("websiteId", "websiteName", "", "", "title", null as unknown as Flex)
    // });
  });

  it('has proper class attribute', () => {
    // @ts-ignore
    const wrapper = shallowMount(CmsHeading, {propsData: defaultProps});
    // expect(wrapper.vm.getClass()).toEqual(["text-h2", "borderedWhenHovered", "show_wysiwyg_editor_hint"])
    //expect(typeof wrapper.vm.showEditor).toBe('boolean');
  });


});
