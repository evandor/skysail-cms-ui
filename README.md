<img src="https://img.shields.io/npm/v/quasar-ui-skysail-cms-ui.svg?label=quasar-ui-skysail-cms-ui">
<img src="https://img.shields.io/npm/v/quasar-app-extension-skysail-cms-ui.svg?label=quasar-app-extension-skysail-cms-ui">

Compatible with Quasar UI v2 and Vue 3.

# Structure
* [/ui](ui) - standalone npm package
* [/app-extension](app-extension) - Quasar app extension

# Donate
If you appreciate the work that went into this project, please consider [donating to Quasar](https://donate.quasar.dev).

# License
MIT (c) evandor <evandor@gmail.com>
