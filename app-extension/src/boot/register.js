import { boot } from 'quasar/wrappers'
import VuePlugin from 'quasar-ui-skysail-cms-ui'

export default boot(({ app }) => {
  app.use(VuePlugin)
})
